package com.mrvitt.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "user_selling_tbl")
public class UserVehicleSellar {
	
	@Id
	@GenericGenerator(name = "myGen",strategy = "com.mrvitt.util.IdGenerator")
	@GeneratedValue(generator = "myGen")
	@Column(nullable = false)
	private String user_sellar_vehicle_id;
	
	private String name;
	private String model;
	private int original_price;
	private int selling_price;
	private String vehicle_type;
	private String vehicle_no;
	private double amount_paid;
	private String company_Id;
	public String getCompany_Id() {
		return company_Id;
	}

	public void setCompany_Id(String company_Id) {
		this.company_Id = company_Id;
	}

	public double getAmount_paid() {
		return amount_paid;
	}

	public void setAmount_paid(double amount_paid) {
		this.amount_paid = amount_paid;
	}

	private String color;
	@Column
	private boolean booked = false;
	
	@OneToMany(mappedBy = "vehicle",orphanRemoval=true,cascade = CascadeType.MERGE)
	private List<Images> images ;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_Id")
	private User user;

	/**
	 * @return the userSellar_vehicleId
	 */
	public String getUser_sellar_vehicle_id() {
		return user_sellar_vehicle_id;
	}

	/**
	 * @param userSellar_vehicleId the userSellar_vehicleId to set
	 */
	public void setUser_sellar_vehicle_id(String user_sellar_vehicle_id) {
		this.user_sellar_vehicle_id = user_sellar_vehicle_id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}

	/**
	 * @param model the model to set
	 */
	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * @return the original_price
	 */
	public int getOriginal_price() {
		return original_price;
	}

	/**
	 * @param original_price the original_price to set
	 */
	public void setOriginal_price(int original_price) {
		this.original_price = original_price;
	}

	/**
	 * @return the selling_price
	 */
	public int getSelling_price() {
		return selling_price;
	}

	/**
	 * @param selling_price the selling_price to set
	 */
	public void setSelling_price(int selling_price) {
		this.selling_price = selling_price;
	}

	/**
	 * @return the vehicle_type
	 */
	public String getVehicle_type() {
		return vehicle_type;
	}

	/**
	 * @param vehicle_type the vehicle_type to set
	 */
	public void setVehicle_type(String vehicle_type) {
		this.vehicle_type = vehicle_type;
	}

	/**
	 * @return the vehicle_no
	 */
	public String getVehicle_no() {
		return vehicle_no;
	}

	/**
	 * @param vehicle_no the vehicle_no to set
	 */
	public void setVehicle_no(String vehicle_no) {
		this.vehicle_no = vehicle_no;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the booked
	 */
	public boolean isBooked() {
		return booked;
	}

	/**
	 * @param booked the booked to set
	 */
	public void setBooked(boolean booked) {
		this.booked = booked;
	}

	/**
	 * @return the images
	 */
	public List<Images> getImages() {
		return images;
	}

	/**
	 * @param images the images to set
	 */
	public void setImages(List<Images> images) {
		this.images = images;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	

	
}
