package com.mrvitt.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "emi_payments_tbl")
public class EmiPayments {

	
	@Id
	@GenericGenerator(name = "myGen",strategy = "com.mrvitt.util.IdGenerator")
	@GeneratedValue(generator = "myGen")
	@Column(nullable = false)
	private String Emi_payments_Id;
	private double remaining_amount;
	private Date date;
	private double months;
	private double sellingprice;
	
	@ManyToOne
	@JoinColumn(name = "buyer_Id")
	private BuyerInfo buyerInfo;
	
	private double amount_paid;
	
	private double perMonth;

	/**
	 * @return the perMonth
	 */
	public double getPerMonth() {
		return perMonth;
	}

	/**
	 * @param perMonth the perMonth to set
	 */
	public void setPerMonth(double perMonth) {
		this.perMonth = perMonth;
	}

	/**
	 * @return the emi_payments_Id
	 */
	public String getEmi_payments_Id() {
		return Emi_payments_Id;
	}

	/**
	 * @param emi_payments_Id the emi_payments_Id to set
	 */
	public void setEmi_payments_Id(String emi_payments_Id) {
		Emi_payments_Id = emi_payments_Id;
	}

	/**
	 * @return the remaining_amount
	 */
	public double getRemaining_amount() {
		return remaining_amount;
	}

	/**
	 * @param remaining_amount the remaining_amount to set
	 */
	public void setRemaining_amount(double remaining_amount) {
		this.remaining_amount = remaining_amount;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the months
	 */
	public double getMonths() {
		return months;
	}

	/**
	 * @param months the months to set
	 */
	public void setMonths(double months) {
		this.months = months;
	}

	/**
	 * @return the sellingprice
	 */
	public double getSellingprice() {
		return sellingprice;
	}

	/**
	 * @param sellingprice the sellingprice to set
	 */
	public void setSellingprice(double sellingprice) {
		this.sellingprice = sellingprice;
	}

	/**
	 * @return the buyerInfo
	 */
	public BuyerInfo getBuyerInfo() {
		return buyerInfo;
	}

	/**
	 * @param buyerInfo the buyerInfo to set
	 */
	public void setBuyerInfo(BuyerInfo buyerInfo) {
		this.buyerInfo = buyerInfo;
	}

	/**
	 * @return the amount_paid
	 */
	public double getAmount_paid() {
		return amount_paid;
	}

	/**
	 * @param amount_paid the amount_paid to set
	 */
	public void setAmount_paid(double amount_paid) {
		this.amount_paid = amount_paid;
	}
	

	
	
	
}
