package com.mrvitt.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "vehicle_tbl")
public class Vehicle {

	@Id
	@GenericGenerator(name = "myGen", strategy = "com.mrvitt.util.IdGenerator")
	@GeneratedValue(generator = "myGen")
	@Column(nullable = false)
	private String vehicle_Id;

	private String name;
	private String model;
	private int original_price;
	private int selling_price;
	private String vehicle_type;
	private String vehicle_role;
	private String vehicle_no;
	private String color;


	public String getVehicle_role() {
		return vehicle_role;
	}

	public void setVehicle_role(String vehicle_role) {
		this.vehicle_role = vehicle_role;
	}


//	@OneToMany(mappedBy = "vehicle", orphanRemoval = true)
//	private List<Images> images;

	
	@Column
	private boolean booked = false;
	
	@OneToMany(mappedBy = "vehicle",cascade = {CascadeType.ALL})
	private List<Images> images ;


	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "company_id")
	private Company company;

	private String user_Id = "No";
	

	/**
	 * @return the user_Id
	 */
	public String getUser_Id() {
		return user_Id;
	}

	/**
	 * @param user_Id the user_Id to set
	 */
	public void setUser_Id(String user_Id) {
		this.user_Id = user_Id;
	}

	/**
	 * @return the vehicle_Id
	 */
	public String getVehicle_Id() {
		return vehicle_Id;
	}

	/**
	 * @param vehicle_Id the vehicle_Id to set
	 */
	public void setVehicle_Id(String vehicle_Id) {
		this.vehicle_Id = vehicle_Id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}

	/**
	 * @param model the model to set
	 */
	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * @return the original_price
	 */
	public int getOriginal_price() {
		return original_price;
	}

	/**
	 * @param original_price the original_price to set
	 */
	public void setOriginal_price(int original_price) {
		this.original_price = original_price;
	}

	/**
	 * @return the selling_price
	 */
	public int getSelling_price() {
		return selling_price;
	}

	/**
	 * @param selling_price the selling_price to set
	 */
	public void setSelling_price(int selling_price) {
		this.selling_price = selling_price;
	}

	/**
	 * @return the vehicle_type
	 */
	public String getVehicle_type() {
		return vehicle_type;
	}

	/**
	 * @param vehicle_type the vehicle_type to set
	 */
	public void setVehicle_type(String vehicle_type) {
		this.vehicle_type = vehicle_type;
	}

	/**
	 * @return the vehicle_no
	 */
	public String getVehicle_no() {
		return vehicle_no;
	}

	/**
	 * @param vehicle_no the vehicle_no to set
	 */
	public void setVehicle_no(String vehicle_no) {
		this.vehicle_no = vehicle_no;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the images
	 */
	public List<Images> getImages() {
		return null;
	}

	/**
	 * @param images the images to set
	 */
	public void setImages(List<Images> images) {
		this.images = images;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return null;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}



	/**
	 * @return the booked
	 */
	public boolean isBooked() {
		return booked;
	}

	/**
	 * @param booked the booked to set
	 */
	public void setBooked(boolean booked) {
		this.booked = booked;
	}

	
}
