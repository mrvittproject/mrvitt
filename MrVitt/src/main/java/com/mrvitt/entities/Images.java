package com.mrvitt.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "image_tbl")
public class Images {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int image_Id;
	private String image_name;
	
	@ManyToOne
	@JoinColumn(name = "vehicle_Id")
	private Vehicle vehicle;

	@ManyToOne
	@JoinColumn(name = "userSellar_vehicleId")
	private UserVehicleSellar userSellar;
	
	/**
	 * @return the image_Id
	 */
	public int getImage_Id() {
		return image_Id;
	}

	/**
	 * @param image_Id the image_Id to set
	 */
	public void setImage_Id(int image_Id) {
		this.image_Id = image_Id;
	}

	/**
	 * @return the image_name
	 */
	public String getImage_name() {
		return image_name;
	}

	/**
	 * @param image_name the image_name to set
	 */
	public void setImage_name(String image_name) {
		this.image_name = image_name;
	}

	/**
	 * @return the vehicle
	 */
	public Vehicle getVehicle() {
		return vehicle;
	}

	/**
	 * @param vehicle the vehicle to set
	 */
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	
	
	
	
}
