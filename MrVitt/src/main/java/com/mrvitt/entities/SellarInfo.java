package com.mrvitt.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Sellars_tbl")
public class SellarInfo {

	@Id
	@GenericGenerator(name = "myGen", strategy = "com.mrvitt.util.IdGenerator")
	@GeneratedValue(generator = "myGen")
	@Column(nullable = false)
	private String sellar_Id;

	@OneToOne
	@JoinColumn(name = "user_Id")
	private User user;

	@ManyToOne
	@JoinColumn(name = "vehicle_Id")
	private Vehicle vehicle;

	@Column
	private float Total_AmountPaid;

	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	/**
	 * @return the sellar_Id
	 */
	public String getSellar_Id() {
		return sellar_Id;
	}

	/**
	 * @param sellar_Id the sellar_Id to set
	 */
	public void setSellar_Id(String sellar_Id) {
		this.sellar_Id = sellar_Id;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the vehicle
	 */
	public Vehicle getVehicle() {
		return null;
	}

	/**
	 * @param vehicle the vehicle to set
	 */
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

}
