package com.mrvitt.util;

public enum UserRoles {
    
	user("User"),financier("Financier");
	
	String value;
	
    private UserRoles(String value) {
		this.value = value;
	}
	
	public String getValue(String value) {
		return value;
	}
	
	
}
