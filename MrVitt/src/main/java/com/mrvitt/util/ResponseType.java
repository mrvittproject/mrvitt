package com.mrvitt.util;

import org.springframework.stereotype.Component;

@Component
public class ResponseType {
   private Object response;
   private int statusCode;
public Object getResponse() {
	return response;
}
public void setResponse(Object response) {
	this.response = response;
}
public int getStatusCode() {
	return statusCode;
}
public void setStatusCode(int statusCode) {
	this.statusCode = statusCode;
}
}
