package com.mrvitt.util;

import java.io.Serializable;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

/**
 * this class is used to create a auto generated values for the all auto
 * increment variables
 * 
 * @author IMVIZAG
 *
 */

public class IdGenerator implements IdentifierGenerator {

	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {

		Connection con = session.connection();
		String newId = "";
		int idPostfix = 1001;
		ResultSet rs = null;
		Statement st = null;
		String prefix = "";
		String lastId = "";
		try {
			st = con.createStatement();
			switch (object.getClass().getName()) {
			case "com.mrvitt.entities.User":
				prefix = "User";
				rs = st.executeQuery("select max(user_id) as max from user_tbl");
				
				break;
				
			case "com.mrvitt.entities.Company":
				prefix = "Company";
				rs = st.executeQuery("select max(company_Id) as max from company_tbl");
				
				break;
			
			case "com.mrvitt.entities.Vehicle":
				prefix = "Vehicle";
				rs = st.executeQuery("select max(vehicle_Id) as max from vehicle_tbl");
				
				break;

			case "com.mrvitt.entities.BuyerInfo":
				prefix = "Buyer";
				rs = st.executeQuery("select max(buyer_Id) as max from Buyers_tbl");
				
				break;
				
			case "com.mrvitt.entities.SellarInfo":
				prefix = "SellarInfo";
				rs = st.executeQuery("select max(sellar_Id) as max from Sellars_tbl");
				
				break;
				
			case "com.mrvitt.entities.EmiPayments":
				prefix = "EmiInfo";
				rs = st.executeQuery("select max(emi_payments_id) as max from emi_payments_tbl");
				break;
				
			case "com.mrvitt.entities.UserVehicleSellar":
				prefix = "US";
				rs = st.executeQuery("select max(user_sellar_vehicle_id) as max from user_selling_tbl");
				break;
				
			default:
				break;
			}
			// ADDING total id
			
			lastId = getLastId(rs);
			
			idPostfix = lastId != null ? Integer.parseInt(lastId.replace(prefix, "")) + 1 : 1001;
			newId = prefix + idPostfix;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return newId;
	}

	/**
	 * this method returns the last id
	 * 
	 * @param rs
	 * @return
	 */
	public String getLastId(ResultSet rs) {
		try {
			if (rs.next()) {
				return rs.getString("max");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
