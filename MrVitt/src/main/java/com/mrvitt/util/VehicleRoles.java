package com.mrvitt.util;

/**
 * this class is used to set the Vehicle Roles
 * 
 * @author Mr.Vitt
 *
 */
public enum VehicleRoles {

	Old("old"), New("new");

	String value;

	private VehicleRoles(String value) {
		this.value = value;
	}

	public String getValue(String value) {
		return value;
	}

}
