package com.mrvitt;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;

import com.mrvitt.services.UserService;
import com.mrvitt.util.MyFailureHandler;
import com.mrvitt.util.MySuccessHandler;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityProvider extends WebSecurityConfigurerAdapter {


	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(getUserService());
		authProvider.setPasswordEncoder(new BCryptPasswordEncoder());
		return authProvider;
	}
    @Bean
    public UserService getUserService() {
    	return new UserService();
    }
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();
		http.cors().configurationSource(request -> new CorsConfiguration().applyPermitDefaultValues());
		http.
		authorizeRequests()
		.antMatchers("/userRegistration/**")
		.permitAll()
		.anyRequest()
		.permitAll() 
		.and()
		.formLogin()
		.successHandler(new MySuccessHandler())
		.failureHandler(new MyFailureHandler())
		.and()
		.httpBasic();
		
	}

}
