package com.mrvitt.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mrvitt.entities.BuyerInfo;

/**
 * This Interface is for BuyersInfoRepository
 * 
 * @author Mr.Vitt
 *
 */
public interface BuyersInfoRepository extends JpaRepository<BuyerInfo, String> {

	@Query("from BuyerInfo where company.company_Id=:company_id")
	List<BuyerInfo> getBuyersInfo(String company_id);
	
	
	@Query("from BuyerInfo where user.user_Id=:user_Id and vehicle.vehicle_Id=:vehicle_Id")
	List<BuyerInfo> findBuyerId(String vehicle_Id, String user_Id);

}
