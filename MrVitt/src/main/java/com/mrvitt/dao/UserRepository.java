package com.mrvitt.dao;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mrvitt.entities.User;


/**
 * This Interface is for UserRepository
 * 
 * @author Mr.Vitt
 *
 */
public interface UserRepository extends JpaRepository<User, String> {
	
	@Query("from User where email=:email")
	public User findByEmail(String email);
	

}
