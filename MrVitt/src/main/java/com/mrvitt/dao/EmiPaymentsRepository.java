package com.mrvitt.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mrvitt.entities.EmiPayments;

public interface EmiPaymentsRepository extends JpaRepository<EmiPayments, String> {

	@Query("from EmiPayments where buyerInfo.buyer_Id=:buyer_Id  ")
	List<EmiPayments> getEmiPayments(String buyer_Id);

}
