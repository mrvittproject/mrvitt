package com.mrvitt.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mrvitt.entities.SellarInfo;

/**
 * This Interface is for SellarInfoRepository
 * 
 * @author Mr.Vitt
 *
 */
public interface SellarInfoRepository extends JpaRepository<SellarInfo, String> {

}
