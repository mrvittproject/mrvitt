package com.mrvitt.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.mrvitt.entities.Company;

/**
 * This Interface is for CompanyRepository
 * 
 * @author Mr.Vitt
 *
 */
public interface CompanyRepository extends JpaRepository<Company, String> {

	@Query("from Company where company_name like concat(company_name,'%')")
	public List<Company> findByCompanyName(String company_name);

	@Query("from Company where owner.user_Id =:user_Id")
	public Optional<Company> findCompanyDetails(String user_Id);

}
