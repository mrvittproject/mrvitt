package com.mrvitt.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mrvitt.entities.UserVehicleSellar;

/**
 * This Interface is for UserVehicleRepository
 * 
 * @author Mr.Vitt
 *
 */
public interface UserVehicleRepository  extends JpaRepository<UserVehicleSellar, String>{

}
