package com.mrvitt.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.mrvitt.entities.Vehicle;

/**
 * This Interface is for VehicleRepository
 * 
 * @author Mr.Vitt
 *
 */
public interface VehicleRepository extends JpaRepository<Vehicle, String>  {

	@Query("UPDATE Vehicle v set v.booked =:isbooked  WHERE v.vehicle_Id =:vehicleNumber")
	Vehicle updateBookingStatus(String vehicleNumber, boolean isbooked);

	
	@Query("from Vehicle where name like concat(name,'%')")
	public List<Vehicle> findByVehicleName(String name);

	
	@Query("from Vehicle where (name =:name) or (color =:color) or  (name =:name and color =:color)")
	public List<Vehicle> findByVehicleColor(String name,String color);

    @Query("from Vehicle where (company_id=:company_id) AND (vehicle_role=:vehicle_role)")
	public List<Vehicle> findByVehicleRole(String company_id, String vehicle_role);


	 @Query("from Vehicle where company_id =:company_id and booked =:booked")
    public List<Vehicle> findByisBooked(String company_id,boolean booked);
	 
	@Query("from Vehicle where user_Id =:user_Id")
	public List<Vehicle> findVehicleDetails(String user_Id);

	@Query("from Vehicle where user_Id=:user_Id")
	List<Vehicle> findVehiclesByUserId(String user_Id);
		

}
