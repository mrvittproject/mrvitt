package com.mrvitt.dto;


public class VehicleResponsedto {

	
private String vehicle_Id;
	
	private String name;
	private String model;
	private int original_price;
	private int selling_price;
	private String vehicle_type;
	private String vehicle_no;
	private String color;
	private float rate_of_interest;
	
	public String getVehicle_Id() {
		return vehicle_Id;
	}
	public void setVehicle_Id(String vehicle_Id) {
		this.vehicle_Id = vehicle_Id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getOriginal_price() {
		return original_price;
	}
	public void setOriginal_price(int original_price) {
		this.original_price = original_price;
	}
	public int getSelling_price() {
		return selling_price;
	}
	public void setSelling_price(int selling_price) {
		this.selling_price = selling_price;
	}
	public String getVehicle_type() {
		return vehicle_type;
	}
	public void setVehicle_type(String vehicle_type) {
		this.vehicle_type = vehicle_type;
	}
	public String getVehicle_no() {
		return vehicle_no;
	}
	public void setVehicle_no(String vehicle_no) {
		this.vehicle_no = vehicle_no;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public float getRate_of_interest() {
		return rate_of_interest;
	}
	public void setRate_of_interest(float rate_of_interest) {
		this.rate_of_interest = rate_of_interest;
	}
	
}
