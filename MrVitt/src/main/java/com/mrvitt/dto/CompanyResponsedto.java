package com.mrvitt.dto;

import java.util.Date;
import java.util.List;
import com.mrvitt.entities.Vehicle;

public class CompanyResponsedto {
	
	
	private String company_Id;

	private String company_name;
	
	private String vehicle_type;
	
	private String company_image;
	
	private String city;
	
	private String state;
	
	private Date date;
	
	private float rate_of_interest;
	
	private float penalty;
	
	private UserResponsedto owner;
	
	private String Owner_name;
	
	/**
	 * @return the owner_name
	 */
	public String getOwner_name() {
		return Owner_name;
	}

	/**
	 * @param owner_name the owner_name to set
	 */
	public void setOwner_name(String owner_name) {
		Owner_name = owner_name;
	}

	
	
	
	
	private List<Vehicle> vehicles;

	/**
	 * @return the company_Id
	 */
	public String getCompany_Id() {
		return company_Id;
	}

	/**
	 * @param company_Id the company_Id to set
	 */
	public void setCompany_Id(String company_Id) {
		this.company_Id = company_Id;
	}

	/**
	 * @return the company_name
	 */
	public String getCompany_name() {
		return company_name;
	}

	/**
	 * @param company_name the company_name to set
	 */
	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

	/**
	 * @return the vehicle_type
	 */
	public String getVehicle_type() {
		return vehicle_type;
	}

	/**
	 * @param vehicle_type the vehicle_type to set
	 */
	public void setVehicle_type(String vehicle_type) {
		this.vehicle_type = vehicle_type;
	}

	/**
	 * @return the company_image
	 */
	public String getCompany_image() {
		return company_image;
	}

	/**
	 * @param company_image the company_image to set
	 */
	public void setCompany_image(String company_image) {
		this.company_image = company_image;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the rate_of_interest
	 */
	public float getRate_of_interest() {
		return rate_of_interest;
	}

	/**
	 * @param rate_of_interest the rate_of_interest to set
	 */
	public void setRate_of_interest(float rate_of_interest) {
		this.rate_of_interest = rate_of_interest;
	}

	/**
	 * @return the penalty
	 */
	public float getPenalty() {
		return penalty;
	}

	/**
	 * @param penalty the penalty to set
	 */
	public void setPenalty(float penalty) {
		this.penalty = penalty;
	}

	/**
	 * @return the owner
	 */
	public UserResponsedto getOwner() {
		return owner;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(UserResponsedto owner) {
		this.owner = owner;
	}

	





	/**
	 * @return the vehicles
	 */
	public List<Vehicle> getVehicles() {
		return vehicles;
	}

	/**
	 * @param vehicles the vehicles to set
	 */
	public void setVehicles(List<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}
	
	

}
