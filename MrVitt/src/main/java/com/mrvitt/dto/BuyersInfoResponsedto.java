package com.mrvitt.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.mrvitt.entities.Company;
import com.mrvitt.entities.EmiPayments;
import com.mrvitt.entities.Vehicle;

public class BuyersInfoResponsedto {

	
	
	private String buyer_Id;
	
	
	private float down_payment;
	
	private Date date;
	
	private int installment_period;

	
	private List<EmiPayments> instalments = new ArrayList<EmiPayments>();

	
	private Company company;


	private UserResponsedto user;


	private Vehicle vehicle;


	/**
	 * @return the buyer_Id
	 */
	public String getBuyer_Id() {
		return buyer_Id;
	}


	/**
	 * @param buyer_Id the buyer_Id to set
	 */
	public void setBuyer_Id(String buyer_Id) {
		this.buyer_Id = buyer_Id;
	}


	/**
	 * @return the down_payment
	 */
	public float getDown_payment() {
		return down_payment;
	}


	/**
	 * @param down_payment the down_payment to set
	 */
	public void setDown_payment(float down_payment) {
		this.down_payment = down_payment;
	}


	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}


	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}


	/**
	 * @return the installment_period
	 */
	public int getInstallment_period() {
		return installment_period;
	}


	/**
	 * @param installment_period the installment_period to set
	 */
	public void setInstallment_period(int installment_period) {
		this.installment_period = installment_period;
	}


	/**
	 * @return the instalments
	 */
	public List<EmiPayments> getInstalments() {
		return instalments;
	}


	/**
	 * @param instalments the instalments to set
	 */
	public void setInstalments(List<EmiPayments> instalments) {
		this.instalments = instalments;
	}


	/**
	 * @return the company
	 */
	public Company getCompany() {
		return null;
	}


	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}


	/**
	 * @return the user
	 */
	public UserResponsedto getUser() {
		
		return user ;
	}

	
	/**
	 * @param user the user to set
	 */
	public void setUser(UserResponsedto user) {
		this.user = user;
	}

}
