package com.mrvitt.dto;

import java.util.List;



public class UserEmidto {

	private UserResponsedto userDetails;
	private List<VehicleResponsedto> vehicleDetails;
	private List<BuyersInfoResponsedto> buyerDetails;
	/**
	 * @return the userDetails
	 */
	public UserResponsedto getUserDetails() {
		return userDetails;
	}
	/**
	 * @param userDetails the userDetails to set
	 */
	public void setUserDetails(UserResponsedto userDetails) {
		this.userDetails = userDetails;
	}
	/**
	 * @return the vehicleDetails
	 */
	public List<VehicleResponsedto> getVehicleDetails() {
		return vehicleDetails;
	}
	/**
	 * @param vehicleDetails the vehicleDetails to set
	 */
	public void setVehicleDetails(List<VehicleResponsedto> vehicleDetails) {
		this.vehicleDetails = vehicleDetails;
	}
	/**
	 * @return the buyerDetails
	 */
	public List<BuyersInfoResponsedto> getBuyerDetails() {
		return buyerDetails;
	}
	/**
	 * @param buyerDetails the buyerDetails to set
	 */
	public void setBuyerDetails(List<BuyersInfoResponsedto> buyerDetails) {
		this.buyerDetails = buyerDetails;
	}
	
	
}
