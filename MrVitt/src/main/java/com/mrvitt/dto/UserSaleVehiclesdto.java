package com.mrvitt.dto;

import java.util.List;
import com.mrvitt.entities.Images;


public class UserSaleVehiclesdto {
	
private String user_sellar_vehicle_id;
	
	private String name;
	private String model;
	private int original_price;
	private int selling_price;
	private String vehicle_type;
	private String vehicle_no;
	private String color;
	
	private boolean booked = false;
	
	
	private List<Images> images ;
	private UserResponsedto user;
	/**
	 * @return the user_sellar_vehicle_id
	 */
	public String getUser_sellar_vehicle_id() {
		return user_sellar_vehicle_id;
	}
	/**
	 * @param user_sellar_vehicle_id the user_sellar_vehicle_id to set
	 */
	public void setUser_sellar_vehicle_id(String user_sellar_vehicle_id) {
		this.user_sellar_vehicle_id = user_sellar_vehicle_id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}
	/**
	 * @param model the model to set
	 */
	public void setModel(String model) {
		this.model = model;
	}
	/**
	 * @return the original_price
	 */
	public int getOriginal_price() {
		return original_price;
	}
	/**
	 * @param original_price the original_price to set
	 */
	public void setOriginal_price(int original_price) {
		this.original_price = original_price;
	}
	/**
	 * @return the selling_price
	 */
	public int getSelling_price() {
		return selling_price;
	}
	/**
	 * @param selling_price the selling_price to set
	 */
	public void setSelling_price(int selling_price) {
		this.selling_price = selling_price;
	}
	/**
	 * @return the vehicle_type
	 */
	public String getVehicle_type() {
		return vehicle_type;
	}
	/**
	 * @param vehicle_type the vehicle_type to set
	 */
	public void setVehicle_type(String vehicle_type) {
		this.vehicle_type = vehicle_type;
	}
	/**
	 * @return the vehicle_no
	 */
	public String getVehicle_no() {
		return vehicle_no;
	}
	/**
	 * @param vehicle_no the vehicle_no to set
	 */
	public void setVehicle_no(String vehicle_no) {
		this.vehicle_no = vehicle_no;
	}
	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}
	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}
	/**
	 * @return the booked
	 */
	public boolean isBooked() {
		return booked;
	}
	/**
	 * @param booked the booked to set
	 */
	public void setBooked(boolean booked) {
		this.booked = booked;
	}
	/**
	 * @return the images
	 */
	public List<Images> getImages() {
		return images;
	}
	/**
	 * @param images the images to set
	 */
	public void setImages(List<Images> images) {
		this.images = images;
	}
	/**
	 * @return the user
	 */
	public UserResponsedto getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(UserResponsedto user) {
		this.user = user;
	}

	
	
	
}
