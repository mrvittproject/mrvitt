package com.mrvitt.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mrvitt.dao.UserVehicleRepository;
import com.mrvitt.entities.UserVehicleSellar;
@Transactional
@Service
public class UserVehicleService {

	@Autowired
	private UserVehicleRepository userVehicleRepository;
	
	public int addUserVehicle(UserVehicleSellar userVehicle) {
			
		UserVehicleSellar userSellar = userVehicleRepository.save(userVehicle);
		if(userSellar != null) {
			return 1;
		} else {
			return -1;
		}
	}

	public List<UserVehicleSellar> getAllVehicles() {
		try {
		return userVehicleRepository.findAll();
		} catch (Exception e){
			return null;
		}
		
	}
	
	public Optional<UserVehicleSellar> getVehicleDetailsById(String user_sellar_vehicle_id) {
		Optional<UserVehicleSellar> userVehicleDetails = userVehicleRepository.findById(user_sellar_vehicle_id);
		return userVehicleDetails;
	}

	public int bookUserVehicle(UserVehicleSellar userVehicleSellar) {
				UserVehicleSellar userVehicle = userVehicleRepository.save(userVehicleSellar);
		if(userVehicle != null) {
			return 1;
		} else {
			return -1;
		}
	}

}
