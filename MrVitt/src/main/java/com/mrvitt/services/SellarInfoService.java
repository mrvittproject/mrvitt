package com.mrvitt.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mrvitt.dao.SellarInfoRepository;
import com.mrvitt.entities.SellarInfo;

@Service
@Transactional
public class SellarInfoService {

	@Autowired
	private SellarInfoRepository sellarInfoRepo;
	
	public int addSellarInfo(SellarInfo sellarInfo) {
		
		SellarInfo sellar = sellarInfoRepo.save(sellarInfo);
		if(sellar != null ) {
			return 1;
		} else {
			return -1;
		}
		
	}

}
