package com.mrvitt.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mrvitt.dao.CompanyRepository;
import com.mrvitt.entities.Company;


/**
 * This class is used to implement the Service used by the Company
 * 
 * @author Mr.Vitt
 *
 */
@Service
@Transactional
public class CompanyService {

	@Autowired
	private CompanyRepository companyRepository;

	/**
	 * This method is for CompanyRegistration with his details
	 * 
	 * @param company
	 * @return
	 */
	public int companyRegistration(Company company) {

		Date dateOfRegistration = new Date(System.currentTimeMillis());
		company.setDate(dateOfRegistration);
		Company newCompany = companyRepository.save(company);
		if (newCompany != null) {
			return 1;
		}

		else {
			return -1;
		}
	}

	/**
	 * This method is for to display AllCompanies
	 * 
	 * @return
	 */
	public List<Company> getAllCompanies() {
		return companyRepository.findAll();

	}

	/**
	 * This method is for to get the company details by searching with the company_name.
	 * @param company_name
	 * @return
	 */
	public List<Company> findByCompanyName(String company_name) {
		List<Company> com = new ArrayList<Company>();
		List<Company> companyList = companyRepository.findByCompanyName(company_name);
		for (Company company : companyList) {
			System.out.println(company);
			if (company.getCompany_name().startsWith(company_name)) {
				com.add(company);
				System.out.println(company.getCompany_name());
			} else
				System.out.println(company.getCompany_name());
		}

		return com;

	}

	public Optional<Company> getCompanyById(String user_Id) {
		Optional<Company> companyDetails = companyRepository.findCompanyDetails(user_Id);
		return companyDetails;
	}


	public Company findById(String company_Id) {
		Optional<Company> company = companyRepository.findById(company_Id);
		return company.get();
		
	}

}
