package com.mrvitt.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mrvitt.dao.VehicleRepository;
import com.mrvitt.entities.Vehicle;

/**
 * This class is used to implements services used by the vehicle
 * @author Mr.Vitt
 *
 */
@Service
@Transactional
public class VehicleService {

	@Autowired
	private VehicleRepository vehicleRepository;	
	
	@Autowired
	private CompanyService companyService;
	

    
	/**
	 * This method is for VehicleRegistration with vehicle details.
	 * @param vehicle
	 * @return
	 */
	public int vehicleRegistration(Vehicle vehicle) {

	Vehicle vehicleDb	= vehicleRepository.save(vehicle);
	if (vehicleDb!= null) {
		return 1;
	} else {
		return -1;
		}
	}


    
	/**
	 * This method is to displayAllVehicles.
	 * @return
	 */
	public List<Vehicle> getAllVehicles() {

		return vehicleRepository.findAll();
	}

	
	/**
	 * This method is to book vehicle
	 * @param vehicle
	 * @return
	 */
	public int bookVehicle(Vehicle vehicle) {
		Vehicle bookingVehicle = vehicleRepository.save(vehicle);
		if(bookingVehicle != null) {
			return 1;
		} else {
			return -1;
		}
		
	}

    
	/**
	 * This method is to get the Vehicle Details by searching with the vehicle name.
	 * @param name
	 * @return
	 */
	public List<Vehicle> findByVehicleName(String name) {
		List<Vehicle> vehicles = new ArrayList<Vehicle>();
		List<Vehicle> vehicleList = vehicleRepository.findByVehicleName(name);
		for (Vehicle vehicle : vehicleList) {
			System.out.println(vehicle);
			if (vehicle.getName().startsWith(name)) {
				vehicles.add(vehicle);
				System.out.println(vehicle.getName());
			} else
				System.out.println(vehicle.getName());
		}

		return vehicles;

	}
	
	/**
	 * This method is to get the Vehicle Details by searching with the vehicle name or with color.
	 * @param name
	 * @param color
	 * @return
	 */
	public List<Vehicle> findByVehicleColorandName(String name,String color) {
		List<Vehicle> vehicles = new ArrayList<Vehicle>();

		List<Vehicle> vehicleList = vehicleRepository.findByVehicleColor(name, color);

		return vehicleList;
	}
	
	/**
	 * This method is to get the Vehicle Details by searching with the vehicle name.
	 * @param name
	 * @return
	 */
	public List<Vehicle> findByVehicleColorandName(String name) {
		String temp = name;
		System.out.println("color "+ name);
		List<Vehicle> vehicles = new ArrayList<Vehicle>();
		List<Vehicle> vehicleList = vehicleRepository.findByVehicleColor(name, temp);
		System.out.println(vehicleList.size());

		return vehicleList;
		
	}
	
	/**
	 * This method is to get the old and new vehicle by searching with the Company_id and vehicle_role.
	 * @param company_id
	 * @param vehicle_role
	 * @return
	 */
	public List<Vehicle> findByVehicleRole(String company_id,String vehicle_role) {
		List<Vehicle> vehicles = new ArrayList<Vehicle>();
		List<Vehicle> vehicleList = vehicleRepository.findByVehicleRole(company_id, vehicle_role);

		return vehicleList;
		
	}
    
	/**
	 * This method is to get the vehicles data which are booked and not booked in the company.
	 * @param company_id
	 * @param booked
	 * @return
	 */
	public List<Vehicle> isBooked(String company_id,boolean booked) {
		
		List<Vehicle> result = vehicleRepository.findByisBooked(company_id, booked);
		return result;
		
	}
	
	public List<Vehicle> getVehicleById(String user_Id) {
		List<Vehicle> vehicleDetails = vehicleRepository.findVehicleDetails(user_Id);
				//.findById(user_Id);
				//.findCompanyDetails(user_Id);
		return vehicleDetails;
	}



	
	public Vehicle findVehicleDetails(String vehicle_Id) {
	
	Optional<Vehicle> vehicleDetails = vehicleRepository.findById(vehicle_Id);
	 return vehicleDetails.get();
	}



	public List<Vehicle> findVehiclesByUserId(String user_Id) {
	   List<Vehicle> vehicleDetails = vehicleRepository.findVehiclesByUserId(user_Id);
		return vehicleDetails;
	}
	

	
}