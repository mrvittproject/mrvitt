package com.mrvitt.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mrvitt.dao.BuyersInfoRepository;
import com.mrvitt.dao.CompanyRepository;
import com.mrvitt.dao.EmiPaymentsRepository;
import com.mrvitt.dao.UserRepository;

import com.mrvitt.dao.VehicleRepository;
import com.mrvitt.entities.BuyerInfo;
import com.mrvitt.entities.Company;
import com.mrvitt.entities.EmiPayments;
import com.mrvitt.entities.User;
import com.mrvitt.entities.Vehicle;

@Service
@Transactional
public class BuyersInfoService {

	@Autowired
	private BuyersInfoRepository buyersInfoRepo;

	@Autowired
	private EmiPaymentsRepository emiRepository;

	@Autowired
	private CompanyRepository companyRepository;

	@Autowired
	private VehicleRepository vehicleRepository;

	@Autowired
	private UserRepository userRepository;

	public int addBuyerInfo(BuyerInfo buyerInfo) {
		BuyerInfo newBuyerInfo = buyersInfoRepo.save(buyerInfo);
		int result = (newBuyerInfo != null) ? 1 : -1;
		return result;
	}

	public Vehicle getVehicleDetail(String vehicleId) {
		Optional<Vehicle> vehicleDetails = vehicleRepository.findById(vehicleId);
		return vehicleDetails.get();

	}

	public List<BuyerInfo> getBuyersInfo(String company_id) {
		return buyersInfoRepo.getBuyersInfo(company_id);

	}

	public User findDetailsByUserId(String email) {
		User user = userRepository.findByEmail(email);
		return user;
	}

	public int fillEmiPayment(BuyerInfo buyer, double downPayment, double totalAmount, double monthDuration,
			double sellingPrice, double perMonth) {

		EmiPayments emiPayments = new EmiPayments();

		emiPayments.setRemaining_amount(totalAmount);
		emiPayments.setAmount_paid(downPayment);
		emiPayments.setRemaining_amount(totalAmount);
		emiPayments.setMonths(monthDuration);
		emiPayments.setSellingprice(totalAmount);
		emiPayments.setBuyerInfo(buyer);
		emiPayments.setPerMonth(perMonth);
		EmiPayments emiPayment = emiRepository.save(emiPayments);
		BuyerInfo buyerInfo = buyersInfoRepo.save(buyer);
		if (emiPayment != null && buyerInfo != null) {
			return 1;
		} else {
			return -1;
		}

	}

	public List<EmiPayments> geEmiPayments(String buyer_Id) {

		return emiRepository.getEmiPayments(buyer_Id);

	}

	public Company findCompanyDetails(String company_Id) {

		Optional<Company> companyDetails = companyRepository.findById(company_Id);
		return companyDetails.get();
	}

	public Vehicle findvehicleDetails(String vehicle_Id) {

		Optional<Vehicle> vehicleDetails = vehicleRepository.findById(vehicle_Id);
		return vehicleDetails.get();

	}

	public BuyerInfo findBuyerId(String vehicle_Id, String user_Id) {
		List<BuyerInfo> buyerInfo = buyersInfoRepo.findBuyerId(vehicle_Id, user_Id);
		return buyerInfo.get(0);

	}

	public int saveEmiPayments(EmiPayments emiPayments) {
		//emiPayments.setBuyerInfo(buyersInfoRepo.save(emiPayments.getBuyerInfo()));
		EmiPayments saveEmi = emiRepository.save(emiPayments);
		emiRepository.flush();
		if (saveEmi != null) {
			return 1;
		} else {
			return -1;
		}

	}

}
