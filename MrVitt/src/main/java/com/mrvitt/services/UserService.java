package com.mrvitt.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.mrvitt.dao.CompanyRepository;
import com.mrvitt.dao.UserRepository;
import com.mrvitt.dao.VehicleRepository;
import com.mrvitt.dto.SecurityUserDetails;
import com.mrvitt.dto.VehicleResponsedto;
import com.mrvitt.entities.Company;
import com.mrvitt.entities.User;
import com.mrvitt.util.ModelMapperUtil;

/**
 * This class is used to implements services used by the user
 * @author Mr.Vitt
 *
 */
@Service
@Transactional
public class UserService implements UserDetailsService {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private CompanyRepository companyRepository;
	@Autowired
	private VehicleRepository vehicleRepository;
	@Autowired
	private ModelMapperUtil modelMapperUtil;
    
	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		User user = userRepository.findByEmail(userName);
		if (user == null) {
			throw new UsernameNotFoundException("User not registered");
		}

		return new SecurityUserDetails(user);

	}
    
	/**
	 * This method  is for userRegistration with his details
	 * @param user
	 * @return
	 */
	public int userRegistration(User user) {
		User userDb = userRepository.findByEmail(user.getEmail());
		if (userDb == null) {
			userRepository.save(user);
			return 1;
		}
		return -1;
	}

	public User findById(String user_Id) {
		Optional<User> user = userRepository.findById(user_Id);
		return user.get();
	}
    
	/**
	 * This method is for to get AllCompanyVehicles with the company_id
	 * @param company_id
	 * @return
	 */
	public List<VehicleResponsedto> getVehicles(String company_id) {
		Company company = companyRepository.findById(company_id).get();
		System.out.println(company.getVehicles());
		List<VehicleResponsedto> list = (List<VehicleResponsedto>) company.getVehicles().stream()
				.map(vehicle -> modelMapperUtil.convertEntityToDto(vehicle, VehicleResponsedto.class))
				.collect(Collectors.toList());
		Iterator itr = list.iterator();
		List<VehicleResponsedto> vehicleList = new ArrayList<VehicleResponsedto>();
		while (itr.hasNext()) {
			VehicleResponsedto vehicleResponsedto = (VehicleResponsedto) itr.next();
			vehicleResponsedto.setRate_of_interest(company.getRate_of_interest());		
			vehicleList.add(vehicleResponsedto);

		}
		return vehicleList;

	}
    
	/**
	 * This method is for to displayAllUsers. 
	 * @return
	 */
	public List<User> getAllUsers() {

		return userRepository.findAll();

	}
	
	
	public User findByEmail(String email) {
		User userDetails = userRepository.findByEmail(email);
		return userDetails;
	}
	
//	public List<VehicleResponsedto> getVehiclesBooked(String user_id){
//		
//		Iterable<Vehicle> vehilcesList = vehicleRepository.findAll();
//	    return vehiclesBookedbyUser(vehilcesList, user_id);		
//		
//	}
//
//	private List<VehicleResponsedto> vehiclesBookedbyUser(Iterable<Vehicle> vehilcesList, String user_id) {
//	   List<VehicleResponsedto> vehicles = new ArrayList<VehicleResponsedto>();
//	   for(Vehicle vehicle :vehilcesList) {
//		   if(vehicle.getCompany().getVehicles().stream().anyMatch(it -> (it.getUser_Id() == user_id))) {
//			   VehicleResponsedto vehicleResponse = modelMapperUtil.convertEntityToDto(vehicle, VehicleResponsedto.class);
//		       vehicles.add(vehicleResponse);
//		   }
//	   }
//		return vehicles;
//	}
	
}
