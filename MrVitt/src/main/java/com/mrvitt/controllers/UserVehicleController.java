package com.mrvitt.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mrvitt.dto.UserSaleVehiclesdto;
import com.mrvitt.entities.UserVehicleSellar;
import com.mrvitt.services.UserVehicleService;
import com.mrvitt.util.ResponseType;
import com.mrvitt.util.StatusCodes;

@CrossOrigin
@RestController
public class UserVehicleController {
	
@Autowired
private UserVehicleService userVehicleService;

 @Autowired
 private ResponseType responseType;
 
 @Autowired
 private ModelMapper model;
 
 @PostMapping("/UserVehicleSelling")
 public ResponseEntity<ResponseType> userVehicleRegistration(@RequestBody UserVehicleSellar userVehicle) {
	int result =  userVehicleService.addUserVehicle(userVehicle);
		String status = "";
		if(result == 1) {
			status = "success";
			responseType.setStatusCode(StatusCodes.OK.getValue());
		} else {
		    status = "Email already Exits";
		    responseType.setStatusCode(StatusCodes.ERROR.getValue());
		}
		responseType.setResponse(status);
		return new ResponseEntity<ResponseType>(responseType,HttpStatus.OK);
  }

 
 
 @GetMapping("/getUserSaleVehicles")
 	public ResponseEntity<ResponseType> getUserVehiclesForSale(){
 		List<UserVehicleSellar> allUserVehicles	= userVehicleService.getAllVehicles();
 		if(allUserVehicles != null) {	
 		ArrayList<UserSaleVehiclesdto> vehiclesList = (ArrayList<UserSaleVehiclesdto>) allUserVehicles.stream().map(vehicles -> model.map(vehicles, UserSaleVehiclesdto.class)).collect(Collectors.toList());
 			responseType.setResponse(vehiclesList);
 			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
 		} else {
 			responseType.setResponse("Not Found");
 			return new ResponseEntity<ResponseType>(responseType, HttpStatus.NOT_FOUND);
 		}
		
 	}
 
 @GetMapping("/UserVehicleSellar")
	public ResponseEntity<ResponseType> getUserVehicleById(@RequestBody UserVehicleSellar userVehicleSellar) {
	Optional<UserVehicleSellar> userVehicle =	userVehicleService.getVehicleDetailsById(userVehicleSellar.getUser_sellar_vehicle_id());
	System.out.println(userVehicle);		
	UserVehicleSellar uservehicleDetails =	userVehicle.get();
		if(uservehicleDetails != null) {
			responseType.setResponse(model.map(uservehicleDetails, UserSaleVehiclesdto.class));
			responseType.setStatusCode(StatusCodes.OK.getValue());
		} else  {
			responseType.setResponse("Not Found");
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
		}
		 return new ResponseEntity<ResponseType>(responseType,HttpStatus.OK);
		
	}
 
 @PutMapping("/bookUserVehicle")
	public ResponseEntity<ResponseType> bookUserVehicleBy(@RequestBody UserVehicleSellar userVehicleSellar) {
		int result = 0;
		if (userVehicleSellar.getCompany_Id() != null && userVehicleSellar.isBooked()) {
			result = userVehicleService.bookUserVehicle(userVehicleSellar);
			if (result == 1) {
				responseType.setResponse("Booked");
				responseType.setStatusCode(StatusCodes.OK.getValue());
			}
		} else {
			responseType.setResponse("Not Booked");
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
		}
		return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
	}
}
