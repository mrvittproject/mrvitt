package com.mrvitt.controllers;



import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.mrvitt.dto.VehicleResponsedto;
import com.mrvitt.entities.User;
import com.mrvitt.services.UserService;
import com.mrvitt.services.VehicleService;
import com.mrvitt.util.ResponseType;
import com.mrvitt.util.StatusCodes;

/**
 * 
 * This class is used to perform User Operations 
 * @author Mr.Vitt
 *
 */
@CrossOrigin
@RestController
public class UserController {

	@Autowired
	private UserService userService;
    
	@Autowired
	private VehicleService VehicleService;
	
	@Autowired
	private ResponseType responseType;
    
	/**
	 * This method  is for userRegistration with his details
	 * @param user
	 * @return
	 */
	@PostMapping("/userRegistration")
	public ResponseEntity<ResponseType> userRegistration(@RequestBody User user) {

		int result = userService.userRegistration(user);
		String status = "";
		if (result == 1) {
			status = "success";
			responseType.setStatusCode(StatusCodes.OK.getValue());
		} else {
			status = "Email already Exits";
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
		}
		responseType.setResponse(status);

		return new ResponseEntity<ResponseType>(responseType,HttpStatus.OK);
	}
	
	/**
	 * This method is for to displayAllUsers. 
	 * @return
	 */
	@GetMapping("/Allusers")
	public ResponseEntity<ResponseType> displayAllUsers() {
		List<User> allUsers = userService.getAllUsers();

		return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
	}
    
	
	/**
	 * This method is for to get AllCompanyVehicles with the company_id
	 * @param company_id
	 * @return
	 */
	@CrossOrigin
	@GetMapping("/api/vehicles/company/{company_id}")
	public ResponseEntity<ResponseType> getCompanyVehicles(@PathVariable("company_id") String company_id) {

		List<VehicleResponsedto> vehicles = null;
		try {
			vehicles = userService.getVehicles(company_id);

			System.out.println(company_id);

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (vehicles != null) {
			responseType.setResponse(vehicles);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);

		} else {
			responseType.setResponse(StatusCodes.ERROR);
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

	}
   
    @CrossOrigin
	@GetMapping("/api/user/Vehicles/{user_id}")
	public ResponseEntity<ResponseType> getVehicles(@PathVariable("user_id") String user_id) {

		
		List<VehicleResponsedto> vehicles = null;
		try {
			vehicles = userService.getVehicles(user_id);
			System.out.println(user_id);
		} catch (Exception e) {

			e.printStackTrace();
		}
		if (vehicles != null) {
			responseType.setResponse(vehicles);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);

		} else {
			responseType.setResponse(StatusCodes.ERROR);
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}
	}
}
