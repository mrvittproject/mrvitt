package com.mrvitt.controllers;

import java.util.ArrayList;

import java.util.List;
import java.util.Optional;

import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mrvitt.dto.BuyersInfoResponsedto;
import com.mrvitt.dto.CompanyResponsedto;
import com.mrvitt.dto.UserEmidto;
import com.mrvitt.dto.UserResponsedto;
import com.mrvitt.dto.VehicleResponsedto;
import com.mrvitt.entities.BuyerInfo;
import com.mrvitt.entities.Company;
import com.mrvitt.entities.User;
import com.mrvitt.entities.Vehicle;
import com.mrvitt.services.BuyersInfoService;
import com.mrvitt.services.CompanyService;
import com.mrvitt.services.UserService;
import com.mrvitt.services.VehicleService;
import com.mrvitt.util.ResponseType;
import com.mrvitt.util.StatusCodes;



/**
 * This class is for Company operations
 * 
 * @author Mr.Vitt
 *
 */
@CrossOrigin
@RestController
public class CompanyController {

	@Autowired
	private CompanyService companyService;

	@Autowired
	private UserService userService;
	
	@Autowired
	private BuyersInfoService buyerService;
	
	@Autowired
	private VehicleService vehicleService;
	
	@Autowired
	private ResponseType responseType;

	@Autowired
	ModelMapper modelMapper;

	/**
	 * This method is for CompanyRegistration with his details.
	 * 
	 * @param company
	 * @return
	 */
	@PostMapping("/companyRegistration")
	public ResponseEntity<ResponseType> userRegistration(@RequestBody Company company) {

		int result = companyService.companyRegistration(company);
		String status = "";
		if (result == 1) {
			status = company.getCompany_Id();

			responseType.setStatusCode(StatusCodes.OK.getValue());
		} else {
			status = "Not Registered";
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
		}
		responseType.setResponse(status);

		return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
	}

	/**
	 * This method is for to display AllCompanies
	 * 
	 * @return
	 */
	@GetMapping("/AllCompanies")
	public ResponseEntity<ResponseType> displayAllCompanies() {
		List<Company> allCompanies = companyService.getAllCompanies();

		//CompanyResponsedto response = new CompanyResponsedto();

		ArrayList<CompanyResponsedto> companyList = (ArrayList<CompanyResponsedto>) allCompanies.stream()
				.map(company -> modelMapper.map(company, CompanyResponsedto.class)).collect(Collectors.toList());

		responseType.setResponse(companyList);
		return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
	}

	/**
	 * This method is for to get the company details by searching with the
	 * company_name.
	 * 
	 * @param company_name
	 * @return
	 */
	@GetMapping("/getbycompanyname/{company_name}")
	public ResponseEntity<ResponseType> getByName(@PathVariable("company_name") String company_name) {

		java.util.List<Company> list = companyService.findByCompanyName(company_name);

		responseType.setResponse(list);

		return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
	}
    
	/**
	 * This method is to get the company details with the user_Id.
	 * @param user_Id
	 * @return
	 */
//	@GetMapping("/getCompanyDetails/{user_Id}")
//	public ResponseEntity<ResponseType> getCompanyById(@PathVariable("user_Id") String user_Id) {
//		Company company = null;
//		Optional<Company> companyDetails = companyService.getCompanyById(user_Id);
//		try {
//			company = companyDetails.get();
//		} catch (Exception e) {
//			responseType.setResponse("Company Details Not Found");
//			responseType.setStatusCode(StatusCodes.ERROR.getValue());
//			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
//		}
//		System.out.println(company);
//		String status = "";
//		if (company != null) {
//
//			responseType.setResponse(company);
//			responseType.setStatusCode(StatusCodes.OK.getValue());
//		} else {
//			responseType.setResponse("Company Details Not Found");
//			responseType.setStatusCode(StatusCodes.ERROR.getValue());
//		}
//
//		return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
//	}
    
	@GetMapping("/getCompanyDetails/{user_Id}")
	public ResponseEntity<ResponseType> getCompanyById(@PathVariable("user_Id") String user_Id) {
		try {
		Optional<Company> companyDetails = companyService.getCompanyById(user_Id);
	if (companyDetails != null) {
					Company company = companyDetails.get();
				 	CompanyResponsedto companyDto = new CompanyResponsedto();
				 	companyDto.setCompany_Id(company.getCompany_Id());
				 	companyDto.setCompany_name(company.getCompany_name());
				 	companyDto.setRate_of_interest(company.getRate_of_interest());
				 	companyDto.setState(company.getState());
				 	companyDto.setCity(company.getCity());
				 	companyDto.setVehicles(company.getVehicles());
		 	responseType.setResponse(companyDto);
			responseType.setStatusCode(StatusCodes.OK.getValue());
		} else {
			responseType.setResponse("Company Details Not Found");
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
		}
		} catch(Exception e) {
			responseType.setResponse("Company Details Not Found");
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
		}

		return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
	}
	
	
	@GetMapping("/getUserDetails")
	public ResponseEntity<ResponseType> findVehiclesByUserDetails(@RequestBody User user) {
		UserEmidto emiDetails = new UserEmidto();
		List<BuyerInfo> buyerDetails = new ArrayList<BuyerInfo>();
		User userDetails = userService.findByEmail(user.getEmail());
			emiDetails.setUserDetails(modelMapper.map(userDetails, UserResponsedto.class));
			List<Vehicle> vehicleDetails = vehicleService.findVehiclesByUserId(userDetails.getUser_Id());
			ArrayList<VehicleResponsedto> vehicleData = (ArrayList<VehicleResponsedto>) vehicleDetails.stream()
			.map(vehicle -> modelMapper.map(vehicle, VehicleResponsedto.class)).collect(Collectors.toList());
			emiDetails.setVehicleDetails(vehicleData);
			for (Vehicle vehicle : vehicleDetails) {
					BuyerInfo buyerInfo = 	buyerService.findBuyerId(vehicle.getVehicle_Id(),userDetails.getUser_Id());
					buyerDetails.add(buyerInfo);
					}
			ArrayList<BuyersInfoResponsedto> buyerData = (ArrayList<BuyersInfoResponsedto>) buyerDetails.stream()
					.map(buyer -> modelMapper.map(buyer, BuyersInfoResponsedto.class)).collect(Collectors.toList());
			emiDetails.setBuyerDetails(buyerData);
			responseType.setResponse(emiDetails);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
			}
	
}
