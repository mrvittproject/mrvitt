package com.mrvitt.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.mrvitt.entities.Vehicle;
import com.mrvitt.services.VehicleService;
import com.mrvitt.util.ResponseType;
import com.mrvitt.util.StatusCodes;



/**
 * This class is for Vehicle Operations
 * @author Mr.Vitt
 *
 */

@CrossOrigin(origins ="*")
@RestController
public class VehicleController {

	@Autowired
	private VehicleService vehicleService;

	@Autowired
	private ResponseType responseType;
    
	/**
	 * This method is for VehicleRegistration with vehicle details.
	 * @param vehicle
	 * @return
	 */
	@PostMapping("/vehicleRegistration")
	public ResponseEntity<ResponseType> vehicleRegistration(@RequestBody Vehicle vehicle) {
		System.out.println(vehicle.getModel());
		int result = vehicleService.vehicleRegistration(vehicle);
		String status = "";
		if (result == 1) {
			status = "success";
			responseType.setStatusCode(StatusCodes.OK.getValue());
		} else {
			status = "Vehicle Not Added";
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
		}
		responseType.setResponse(status);

		return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
	}
   
	
	/**
	 * This method is to displayAllVehicles.
	 * @return
	 */
	@GetMapping("/displayAllVehicles")
	public ResponseEntity<ResponseType> displayAllVehicles() {
		List<Vehicle> allVehicles = vehicleService.getAllVehicles();
		responseType.setResponse(allVehicles);

		return new ResponseEntity<ResponseType>(responseType,HttpStatus.OK);
}

//	
//	@PutMapping("/bookingVehicle")
//	public ResponseEntity<ResponseType> vehicleBooking(@RequestBody Vehicle vehicle) {
//		String status = "";
//		if (vehicle.isBooked()) {
//			int result = vehicleService.bookVehicle(vehicle);
//				
//			if (result == 1) {
//				status = "Success";
//				responseType.setStatusCode(StatusCodes.OK.getValue());
//			}
//		} else {
//			status = "Vehicle Not Added";
//			responseType.setStatusCode(StatusCodes.ERROR.getValue());
//		}
//		responseType.setResponse(status);
//
//		return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
//
//	}
	@CrossOrigin
	@PutMapping("/bookingVehicle")
	public ResponseEntity<ResponseType> vehicleBooking(@RequestBody Vehicle vehicle) {
				Vehicle vehicleDetails = vehicleService.findVehicleDetails(vehicle.getVehicle_Id());
				vehicleDetails.setBooked(true);
				vehicleDetails.setUser_Id(vehicle.getUser_Id());
		String status = "";
		if (vehicle.isBooked()) {
			
			int result = vehicleService.bookVehicle(vehicleDetails);
				
			if (result == 1) {
				status = "Success";
				responseType.setStatusCode(StatusCodes.OK.getValue());
			}
		} else {
			status = "Vehicle Not Booked";
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
		}
		responseType.setResponse(status);

		return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);

	}

	/**
	 * This method is to get the Vehicle Details by searching with the vehicle name.
	 * @param name
	 * @return
	 */

	@GetMapping("/getbyvehiclename/{color}")
	public ResponseEntity<ResponseType> getByName(@PathVariable("color") String color) {
		
		java.util.List<Vehicle> list = vehicleService.findByVehicleColorandName(color);
		if (list.size() > 0) {
		responseType.setResponse(list);
		responseType.setStatusCode(StatusCodes.OK.getValue());
		return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse(StatusCodes.NOT_FOUND);
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}
	}
	/**
	 * This method is to get the Vehicle Details by searching with the vehicle name or with the vehicle color.
	 * @param name
	 * @param color
	 * @return
	 */
	@GetMapping("/getbyvehiclename/{name}/{color}")
	public ResponseEntity<ResponseType> searchByColor(@PathVariable("name") String name,@PathVariable("color") String color) {

		java.util.List<Vehicle> Vehicleslist = vehicleService.findByVehicleColorandName(name, color);
			
	    System.out.println(color);
		if (Vehicleslist.size() > 0) {
			responseType.setResponse(Vehicleslist);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse(StatusCodes.NOT_FOUND);
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}
	}

	/**
	 * This method is to get the old and new vehicle by searching with the Company_id and vehicle_role.
	 * @param company_id
	 * @param vehicle_role
	 * @return
	 */
	@GetMapping("/getbyvehiclerole/{company_id}/{vehicle_role}")
	public ResponseEntity<ResponseType> searchByVehicleRole(@PathVariable("company_id") String company_id,@PathVariable("vehicle_role") String vehicle_role) {
		 
		java.util.List<Vehicle> Vehicleslist = vehicleService.findByVehicleRole(company_id, vehicle_role);
		
		if(Vehicleslist.size() > 0) {
			responseType.setResponse(Vehicleslist);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse(StatusCodes.NOT_FOUND);
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}
		
	 }
	
	/**
	 * This method is to get the vehicles data which are booked and not booked in the company.
	 * @param company_id
	 * @param booked
	 * @return
	 */
	@GetMapping("/isBooked/{company_id}/{booked}")
	public ResponseEntity<ResponseType> isBooked(@PathVariable("company_id") String company_id,@PathVariable("booked")boolean booked){
		  
		List<Vehicle> Vehicleslist = vehicleService.isBooked(company_id, booked);
		
		if(Vehicleslist != null) {
			responseType.setResponse(Vehicleslist);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse(StatusCodes.NOT_FOUND);
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}
	}
	
	@GetMapping("/getUserVehicle/{user_Id}")
	public ResponseEntity<ResponseType> getVehicleById(@PathVariable("user_Id") String user_Id){
		List<Vehicle> vehiclesList = vehicleService.getVehicleById(user_Id);
		
		if(vehiclesList != null) {
			responseType.setResponse(vehiclesList);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse(StatusCodes.NOT_FOUND);
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}
			
	}
}
