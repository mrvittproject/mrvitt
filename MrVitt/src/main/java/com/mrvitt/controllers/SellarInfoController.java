package com.mrvitt.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mrvitt.entities.SellarInfo;
import com.mrvitt.services.SellarInfoService;
import com.mrvitt.util.ResponseType;
import com.mrvitt.util.StatusCodes;

@CrossOrigin
@RestController
public class SellarInfoController {

	@Autowired
	private SellarInfoService sellarService;
	
	@Autowired
	private ResponseType responseType;
	
	@PostMapping("/addSellar")
	public ResponseEntity<ResponseType> addSellarInfo(@RequestBody SellarInfo sellarInfo) {
		String status = "";
		int result=0;
		try {
		result = sellarService.addSellarInfo(sellarInfo);
		} catch(Exception e){
			status = "User Already Exits";
		    responseType.setStatusCode(StatusCodes.ERROR.getValue());
		}
			
	if(result == 1) {
		status = "success";
		responseType.setStatusCode(StatusCodes.OK.getValue());
			} else {
			    status = "Vehicle Already Exits";
			    responseType.setStatusCode(StatusCodes.ERROR.getValue());
			
		}
		responseType.setResponse(status);
		
		return new ResponseEntity<ResponseType>(responseType,HttpStatus.OK);
	}
	

	}
	

