package com.mrvitt.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mrvitt.dto.BuyersInfoResponsedto;
import com.mrvitt.dto.EmiPaymentsdto;
import com.mrvitt.entities.BuyerInfo;
import com.mrvitt.entities.Company;
import com.mrvitt.entities.EmiPayments;
import com.mrvitt.entities.User;
import com.mrvitt.entities.Vehicle;
import com.mrvitt.services.BuyersInfoService;
import com.mrvitt.services.EmiService;
import com.mrvitt.util.ResponseType;
import com.mrvitt.util.StatusCodes;

@CrossOrigin
@RestController
public class BuyersInfoController {

	@Autowired
	private BuyersInfoService buyersServiceInfo;

	@Autowired
	private EmiService emiService;

	@Autowired
	private ResponseType responseType;

	@Autowired
	ModelMapper modelMapper;

	// method changed
	// @PostMapping("/addBuyerInfo")
	public ResponseEntity<ResponseType> addBuyerInfo(@RequestBody BuyerInfo buyerInfo) {
		int add = 0;
		User userDetails = buyersServiceInfo.findDetailsByUserId(buyerInfo.getUser().getEmail());
		if (userDetails != null) {
			buyerInfo.setUser(userDetails);
			add = buyersServiceInfo.addBuyerInfo(buyerInfo);
		}
		String status = "";
		if (add == 1) {
			status = "success";
			responseType.setStatusCode(StatusCodes.OK.getValue());
		} else {
			status = "Not Added";
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
		}
		responseType.setResponse(status);
		return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
	}

	@GetMapping("/companyBuyersInfo")
	public ResponseEntity<ResponseType> companyByersInfo(@RequestBody Company company) {
		List<BuyerInfo> buyersInfo = buyersServiceInfo.getBuyersInfo(company.getCompany_Id());
		if (buyersInfo != null) {
			ArrayList<BuyersInfoResponsedto> buyerslist = (ArrayList<BuyersInfoResponsedto>) buyersInfo.stream()
					.map(buyers -> modelMapper.map(buyers, BuyersInfoResponsedto.class)).collect(Collectors.toList());
			responseType.setResponse(buyerslist);
			responseType.setStatusCode(StatusCodes.OK.getValue());
		} else {
			responseType.setResponse("NOT_FOUND");
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
		}
		return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
	}

	@PostMapping("/addBuyerInfo")
	public ResponseEntity<ResponseType> buyerPayment(@RequestBody BuyerInfo buyerInfo) {
		User user = buyersServiceInfo.findDetailsByUserId(buyerInfo.getUser().getEmail());
		Company companyDetails = buyersServiceInfo.findCompanyDetails(buyerInfo.getCompany().getCompany_Id());
		buyerInfo.setCompany(companyDetails);
		Vehicle vehicledetails = buyersServiceInfo.findvehicleDetails(buyerInfo.getVehicle().getVehicle_Id());
		buyerInfo.setVehicle(vehicledetails);
		buyerInfo.setUser(user);
		System.out.println(buyerInfo.getVehicle().getSelling_price() + "Selling Price");
		double sellingPrice = buyerInfo.getVehicle().getSelling_price();
		double downPayment = buyerInfo.getDown_payment();
		double remainingAmount = sellingPrice - downPayment;
		double interestPerYear = buyerInfo.getCompany().getRate_of_interest();
		double timePeriod = buyerInfo.getInstallment_period();
		double monthDuration = timePeriod * 12;
		double monthlyInterest = interestPerYear / (12 * 100);
		@SuppressWarnings("unused")
		double power = Math.pow((1 + monthlyInterest / 100), monthDuration);
		double emi = (remainingAmount * monthlyInterest * Math.pow(1 + monthlyInterest, monthDuration))
				/ (Math.pow(1 + monthlyInterest, monthDuration) - 1);
		double totalAmount = emi * monthDuration;
		double perMonth = totalAmount / monthDuration;
		int result = buyersServiceInfo.fillEmiPayment(buyerInfo, downPayment, totalAmount, monthDuration, sellingPrice,
				perMonth);
		if (result == 1) {
			responseType.setResponse("updated");
			responseType.setStatusCode(StatusCodes.OK.getValue());
		} else {
			responseType.setResponse("NOT_FOUND");
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
		}
		return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
	}

	@GetMapping("/getEmiPayments")
	public ResponseEntity<ResponseType> getEmiInstallments(@RequestBody EmiPayments emiPayments) {
		List<EmiPayments> emiDetails = emiService.getEmiPayments(emiPayments.getBuyerInfo().getBuyer_Id());
		ArrayList<EmiPaymentsdto> responseEmi = new ArrayList<EmiPaymentsdto>();
		responseType.setResponse(emiDetails);
		for (EmiPayments emiPaymentsData : emiDetails) {
			EmiPaymentsdto emiPaymentsdto = new EmiPaymentsdto();
			emiPaymentsdto.setEmi_payments_Id(emiPaymentsData.getEmi_payments_Id());
			emiPaymentsdto.setMonths(emiPaymentsData.getMonths());
			emiPaymentsdto.setAmount_paid(emiPaymentsData.getAmount_paid());
			emiPaymentsdto.setRemaining_amount(emiPaymentsData.getRemaining_amount());
			emiPaymentsdto.setDate(emiPaymentsData.getDate());
			emiPaymentsdto.setPerMonth(emiPaymentsData.getPerMonth());
			emiPaymentsdto.setSellingprice(emiPaymentsData.getSellingprice());
			responseEmi.add(emiPaymentsdto);
		}
		responseType.setResponse(responseEmi);
		return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
	}

	@PostMapping("/addEmiPayment")
	public ResponseEntity<ResponseType> addEmiIntstallments(@RequestBody EmiPayments emiPayments) {
		List<EmiPayments> emiDetails = emiService.getEmiPayments(emiPayments.getBuyerInfo().getBuyer_Id());
		EmiPayments emiLatsRecord = emiDetails.get(emiDetails.size() - 1);
		emiPayments.setRemaining_amount(emiLatsRecord.getRemaining_amount() - emiPayments.getAmount_paid());
		emiPayments.setMonths(emiLatsRecord.getMonths() - 1);
		emiPayments.setPerMonth(emiLatsRecord.getPerMonth());
		emiPayments.setSellingprice(emiLatsRecord.getSellingprice());
		int result = buyersServiceInfo.saveEmiPayments(emiPayments);
		if (result == 1) {
			responseType.setResponse("updated");
			responseType.setStatusCode(StatusCodes.OK.getValue());
		} else {
			responseType.setResponse("Error ");
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
		}
		return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
	}

}
