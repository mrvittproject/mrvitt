package com.mrvitt;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.web.firewall.DefaultHttpFirewall;
import org.springframework.security.web.firewall.HttpFirewall;

@SpringBootApplication
public class MrVittApplication {

	public static void main(String[] args) {
		SpringApplication.run(MrVittApplication.class, args);
	}
	
	
	@Bean 
	public ModelMapper getMapper() {
		return new ModelMapper();
	}

	@Bean
	public HttpFirewall defaultHttpFirewall() {
	    return new DefaultHttpFirewall();
	}
}

